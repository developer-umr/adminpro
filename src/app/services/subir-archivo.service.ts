import { URL_SERVICIOS } from './../config/config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubirArchivoService {

  constructor(private _http: HttpClient) { }

  subirArchivo(archivo: File, tipo: string, id: string) {
    return new Promise((resolve, reject) => {
      let formData = new FormData();
      let xhr = new XMLHttpRequest();
      formData.append('imagen', archivo, archivo.name);
      xhr.onreadystatechange = function () {
        // 4= cuando el termina el proceso
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            console.log('imagen subida');
            resolve(JSON.parse(xhr.response));
          } else{
            console.log();
            reject(xhr.response);
          }
        }
      };
      let url = URL_SERVICIOS+'/upload/'+tipo+'/'+id;
      xhr.open('PUT', url, true);
      xhr.send(formData);
    });

    

  }

}
