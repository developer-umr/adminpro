import { SubirArchivoService } from './../subir-archivo.service';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from './../../config/config';
import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/models/user.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import swal from 'sweetalert';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuario: Usuario;
  token: string;

  constructor(private http: HttpClient, private _router: Router, public _subirAcrhivoService: SubirArchivoService) { 
    console.log('Servicio usuarios')
    this.cargarStorage();
  }

  estaLogueado(){
    if(!this.token) return false;
    return (this.token.length>5)?true: false;
  }

  cargarStorage(){
    if(localStorage.getItem('token')){
      this.token = localStorage.getItem('token'),
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    } else{
      this.token ='';
      this.usuario = null;
    }
  }

  guardarStorage(id: string, token:string, usuario:Usuario){
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));

    this.usuario = usuario;
    this.token = token;
  }

  logout(){
    this.usuario = null;
    this.token = '';
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    this._router.navigateByUrl('/login');
  }

  loginGoogle(token: string){
    let url = URL_SERVICIOS + '/login/google';
    // se puede mandar solo el token si key y value so iguales {token:token} = {token} EMSC6
    return this.http.post(url, {token}).pipe(map((resp:any)=>{
      this.guardarStorage(resp.id, resp.token, resp.user);
      return true;
    }));
  }

  login(usuario: Usuario, recordar: boolean = false){
    if(recordar){
      localStorage.setItem('email', usuario.email);
    } else{
      localStorage.removeItem('email');
    }
    let url = URL_SERVICIOS + '/login';
    return this.http.post(url, usuario).pipe(map((resp:any)=>{
      this.guardarStorage(resp.id, resp.token, resp.user);
      return true;
    }));
  }


  crearUsuario(usuario: Usuario){
    let url = URL_SERVICIOS+'/usuario';
    return this.http.post(url, usuario).pipe(map((resp:any)=>{
      swal('Usuario creado', usuario.email, 'success');
     return resp.usuario; 
    }));
  }

  actualizarUsuario(usuario: Usuario){
    let url = URL_SERVICIOS+'/usuario/'+usuario._id;
    url+='?token='+this.token;
    return this.http.put(url, usuario).pipe(map((resp:any)=>{
      let userDB: Usuario = resp.user;
      this.guardarStorage(userDB._id, this.token, userDB);
      swal('Usuario actualizado', userDB.email, 'success');
      return true;
    }));
  }

  cambiarImagen(archivo:File, id:string){
    this._subirAcrhivoService.subirArchivo(archivo,'usuarios', id).then((resp:any)=>{
      console.log('resp: ', resp);
      this.usuario.img = resp.usuario.img;
      swal('Imagen actualizada', this.usuario.nombre, 'success');
      this.guardarStorage(id, this.token, this.usuario);
    }).catch(resp=>{
      console.log('resp: ', resp);
    });
  }
}
