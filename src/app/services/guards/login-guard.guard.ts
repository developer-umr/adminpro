import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {
  constructor(public _usuarioService: UsuarioService, private _router: Router) {
    
  }
  canActivate() {
    if(this._usuarioService.estaLogueado()){
      console.log('Paso el loginGraurd');
      return true;
    } else{
      console.log('Bloqueado por loginGraurd');
      this._router.navigateByUrl('login');
      return false;
    }
  }
  
}
