import { ProfileComponent } from './profile/profile.component';
import { LoginGuardGuard } from './../services/guards/login-guard.guard';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { RouterModule, Routes } from "@angular/router";
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';

const pagesRoute: Routes = [
    {
        path: '', component: PagesComponent, canActivate: [LoginGuardGuard], children: [
            { path: 'dashboard', component: DashboardComponent, data:{titulo: 'Tablero'}},
            { path: 'progress', component: ProgressComponent, data:{titulo: 'Progreso'} },
            { path: 'graficas1', component: Graficas1Component, data:{titulo: 'Gráficas'} },
            { path: 'account-settings', component: AccountSettingsComponent, data:{titulo: 'Cofiguración'}},
            { path: 'profile', component: ProfileComponent, data:{titulo: 'Perfil de usuario'}},
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoute);