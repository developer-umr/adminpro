import { PipesModule } from './../pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { NgModule } from "@angular/core";
import { SidebarComponent } from './sidebar/sidebar.component';
import { BreadcrumsComponent } from './breadcrums/breadcrums.component';

@NgModule({
    imports:[
        RouterModule,
        CommonModule,
        PipesModule
    ],
declarations:[
    HeaderComponent,
    SidebarComponent,
    BreadcrumsComponent
], exports:[
    HeaderComponent,
    SidebarComponent,
    BreadcrumsComponent
]
})
export class SharedModule{}