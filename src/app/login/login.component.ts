import { UsuarioService } from './../services/usuario/usuario.service';
import { URL_SERVICIOS } from './../config/config';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Usuario } from '../models/user.model';
declare function init_plugins();
declare const gapi: any; // hacemos que typescript confie en lo que estamos mandando
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  recuerdame: boolean = false;
  email: string;

  auth2: any;

  constructor(private _router: Router, public _usuarioService: UsuarioService) { }

  ngOnInit() {
    init_plugins();
    this.googleInit();
    this.email = localStorage.getItem('email') || '';
    if (this.email.length > 0) {
      this.recuerdame = true;
    }
  }

  googleInit(){
    gapi.load('auth2', ()=>{
      this.auth2 = gapi.auth2.init({
        client_id: '920027261358-8vkltak6qtcofcbh5tbrn560vbcrfnil.apps.googleusercontent.com',
        cokieppolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignin(document.getElementById('btnGoogle'));
    });
  }

  attachSignin(element){
    this.auth2.attachClickHandler(element, {}, (googleUser)=>{
      let profile = googleUser.getBasicProfile();
      let token = googleUser.getAuthResponse().id_token;
      console.log('profile: ', profile); 
      console.log('token: ', token);
       this._usuarioService.loginGoogle(token).subscribe((resp)=>{
         window.location.href = '/dashboard';
         /// this._router.navigateByUrl('/dashboard');
       });
    });
  }

  ingresar(forma: NgForm) {
    if (!forma) return;
    if (forma.invalid) return;
    let usuario = new Usuario(
      null, forma.value.email, forma.value.password
    );
    console.log('forma valida: ', forma.value);
    this._usuarioService.login(usuario, this.recuerdame).subscribe((result: any) => {
      this._router.navigate(['/dashboard']);
    });

    // this._router.navigateByUrl('/dashboard');
  }

}
